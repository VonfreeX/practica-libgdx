package cat.epiaedu.damviod.pmdm.davidamate;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;

import java.util.Iterator;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;

	private float elapsedTime = 0;
	Texture img;
	Pixmap pixmap;
	TextureAtlas texAtlas;
	Sprite sprite;
	private OrthographicCamera camera;
	Rectangle bird;
	Animation animation;
	boolean pressed=false;
	private Sound idle,running;
	long soundID;
	long soundID2;
	boolean started=false;
	boolean on=false;
	float vel;
	boolean motor_on=false;
	int currentFrame=1;
	private BitmapFont font;
	@Override
	public void create () {
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		batch = new SpriteBatch();
		texAtlas = new TextureAtlas(Gdx.files.internal("sprites-packed/pack.atlas"));
		idle=Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
		running=Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
		animation=new Animation(1/15f,texAtlas.getRegions());
		font = new BitmapFont();

		font.setColor(Color.WHITE);
		//TextureAtlas.AtlasRegion region = texAtlas.findRegion("a1");
		bird=new Rectangle();
		bird.x=800/2-180/2;
		bird.y=20;
		bird.width=32;
		bird.height=32;

		//img = new Texture("sprite.png");
		//sprite=new Sprite(img);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.begin();
		font.draw(batch, Math.round(vel*50)+" Km/h", 200, 200);
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), bird.x, bird.y);
		batch.end();


		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) bird.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) bird.x += 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) bird.y -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.UP)) bird.y += 200 * Gdx.graphics.getDeltaTime();

		// make sure the bucket stays within the screen bounds
		if(bird.x < 0) bird.x = 0;
		if(bird.x > 800 - 64) bird.x = 800 - 64;
		checkMotor();


		}



	void checkMotor() {
		if (Gdx.input.isTouched()&&motor_on) {//check if the motor is on


			bird.x = Gdx.input.getX();
			bird.y = Gdx.graphics.getHeight()-Gdx.input.getY();
			idle.stop();
			on=true;
		}
		else  on=false;
		if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT))//put the motor on
		{
			soundID=idle.play();
			motor_on=true;
			idle.setLooping(soundID,true);
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE))//put it off
		{
			motor_on=false;
			idle.stop();
		}
		if (on) {//if the motor is on
			if (!started) {
				Gdx.app.log("Motor", "started");

				soundID2 = running.play();
				//running.setPitch(soundID2,0);
				running.setLooping(soundID2, true);
				started = true;
			}
			if (vel < 3) {//if the velocity is less than 3
				running.setPitch(soundID2, vel);
				vel = vel + 0.01f;
			}
		} else if (!on && started) {//if the motor is off and it was already started
			vel = vel - 0.01f;
			running.setPitch(soundID2, vel);
			if (vel < 0) {//if the velocity is less than 0 the sound will be stopped
				Gdx.app.log("Motor", "apagado");
				running.setLooping(soundID2, false);
				running.stop();
				soundID=idle.play();
				started = false;
			}
		}
	}
	@Override
	public void dispose () {
		batch.dispose();
		//img.dispose();
		texAtlas.dispose();
		idle.dispose();
		running.dispose();
	}
}
